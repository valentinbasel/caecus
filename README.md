﻿# CAECUS

Software lector de PDFs para personas ciegas usando sistemas GNU/Linux.

## Introducción

CAECUS es una herramienta para leer archivos textuales en formato PDF y poder relatar el texto usando sistemas de síntesis de voz basados en Espeak, festival o el sistema de síntesis de voz de Google.

Trabaja íntegramente desde la terminal GNU/Linux y no necesita en torno gráfico para funcionar.

## Funcionamiento

Para habilitar CAECUS es necesario clonar este repositorio y llamar al archivo main.py con el argumento del archivo a leer.

Una vez abierta la terminal CAECUS nos indicará con un sonido para poder comenzar a leer el archivo. Escribiendo la instrucción “ayuda” + Intro, CAECUS relatará el siguiente texto:

Comandos de caecus:
En caecus, todos los comandos son instrucciones que sé Escriben en una terminal de ordenes, por lo tanto, es Necesario escribir una orden y luego presionar la tecla intro.
Los sonidos ayudan a saber si estamos en la terminal de escritura y si el sistema esta procesando la síntesis o ya termino de pronunciar.
Si escribes "ayuda" explica esta ayuda.
Si ingresas un número hace un salto a una página especificada.
Si presionas la tecla más. Avanza.
Si presionas la tecla menos. Retrocede.
Si escribes los símbolos más más, avanza una página.
Si escribes los símbolos menos menos, retrocede una página.
Si presionas la tecla símbolo de pregunta te dirá información del documento.
Si no escribes nada y presionas INTRO, vuelve a leer el párrafo.
Caescus puede trabajar con 3 motores de síntesis de voz.
Si escribes "espeak", con la letra k al final, usara el motor espeak, que viene habilitado por defecto.
Si escribes "festival", caecus tratara de usar el motor festival.
Si escribes "google", caecus usará el motor google, pero requiere tener internet todo el tiempo.
Recuerda: todos los comandos se escriben y luego debes presionar la tecla INTRO para que se ejecuten, escucha los sonidos para saber si está habilitado para escribir.

## FCC

Caecus fue financiado con fondos de la Facultad de Ciencias de la Comunicación de la Universidad Nacional de Córdoba con El EX-2023-00467279- -UNC-ME#FCC en el Área de Accesibilidad Digital y transferencia en la producción de herramientas digitales para la accesibilidad educativa. 


