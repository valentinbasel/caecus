#!/usr/bin/env python3
# -*- coding: utf-8 -*-

###############################################################################
# 
# Copyright © 2021 Valentin Basel <valentinbasel@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
###############################################################################

import argparse
from pynput import keyboard
import pdftotext

import espeakng
from gtts import gTTS

import sys
import os
import ntpath
import playsound

class LECTOR(object):

    """Docstring for LECTOR. """

    def __init__(self,archivo,sintesis):
        """TODO: to be defined. """
        self.__archivo = archivo
        self.pagina = 0
        self.parrafo = 0
        voz = VOZ(sintesis)
        self.decir = voz.decir
        self.abrir()
        self.sintesis = sintesis

    def abrir(self):
        """TODO: Docstring for abrir.
        :returns: TODO

        """
        with open(self.__archivo, "rb") as f:
            self.pdf = pdftotext.PDF(f)

    def Lpdf(self, band):
        """TODO: Docstring for Lpdf.

        :arg1: TODO
        :returns: TODO

        """
        if self.pagina >= len(self.pdf):
            self.decir("se llego al final del archivo",self.sintesis)
            return False
        if self.pagina < 0 :
            self.decir("se llego al principio del documento",self.sintesis)
            self.pagina = 0

        if band == 1: 
            pdf_fin = self.pdf[self.pagina]
            texto = pdf_fin#self.pdf[self.pagina]#pdf_fin[self.parrafo]
            texto = texto.rstrip("\n")
            #print(texto)
            self.decir(texto,self.sintesis)
            #self.pagina += 1
            #self.parrafo = 0
        else:
            pdf_fin = self.pdf[self.pagina].split(".")
            if self.parrafo < len(pdf_fin):
                print(pdf_fin[self.parrafo])
                texto = pdf_fin[self.parrafo]#self.pdf[self.pagina]#pdf_fin[self.parrafo]
                texto = texto.rstrip("\n")
                #print(texto)
                self.decir(texto,self.sintesis)
                #self.parrafo += 1
                #self.pagina += 1
            if self.parrafo < 0:
                self.decir("pagina anterior",self.sintesis)
                self.pagina -=1
                self.parrafo = 0
            if self.parrafo >= len(pdf_fin):
                self.decir("fin pagina",self.sintesis)
                self.pagina  +=1
                self.parrafo = 0
        return True
        
class NAVEGADOR(object):

    """Docstring for NAVEGADOR. """

    def __init__(self, archivo, sintesis):
        """TODO: to be defined. """
        self.ordenes = ""
        self.voz = VOZ(sintesis)
        self.band = True
        self.sintesis = sintesis
        self.cargar(archivo, sintesis)

    def cargar(self, archivo, sintesis):
        """TODO: Docstring for cargar.

        :a: TODO
        :returns: TODO

        """
        if archivo =="":
            self.voz.decir("no hay archivo",self.sintesis)
            exit()
        else:
            self.pdf = LECTOR(archivo,sintesis)

    def promp(self):
        """TODO: Docstring for orden.
        :returns: TODO

        """

        while self.band == True:
            self.voz.msg_promp()
            text = input(">")
            if text == "ayuda":
                self.ayuda()
            elif text == "espeak":
                self.sintesis = "espeak"
                self.pdf.sintesis = self.sintesis
            elif text == "google":
                self.sintesis = "google"
                self.pdf.sintesis = self.sintesis
            elif text == "festival":
                self.sintesis = "festival"
                self.pdf.sintesis = self.sintesis
            elif text == "*":
                self.pdf.pagina +=1 
                self.pdf.parrafo = 0
                self.pdf.Lpdf(1)
            elif text == "++":
                self.pdf.pagina +=1 
                self.pdf.parrafo = 0
                self.pdf.Lpdf(0)
            elif text == "--":
                self.pdf.pagina -=1 
                self.pdf.parrafo = 0
                self.pdf.Lpdf(0)
            elif text == "+":
                self.pdf.parrafo+=1
                self.pdf.Lpdf(0)
            elif text == "-":
                self.pdf.parrafo-=1
                self.pdf.Lpdf(0)
            elif text.isnumeric()==True:
                self.pdf.pagina = int(text)
                self.pdf.parrafo = 0
                self.pdf.Lpdf(0)
            elif text == "?":
                self.info()
            elif text == "":
                self.pdf.Lpdf(0)
            elif text == "salir":
                self.band = False
            else:
                self.voz.decir("no comprendo la palabra: " + text,self.sintesis)
            #return text
        self.voz.decir("adiós",self.sintesis)
        return text

    def info(self):
        """TODO: Docstring for info.
        :returns: TODO

        """
        self.voz.decir("página "+str(self.pdf.pagina) +
                       ". Párrafo "+str(self.pdf.parrafo),self.sintesis)

    def ayuda(self):
        """TODO: Docstring for ayuda.

        :arg1: TODO
        :returns: TODO

        """
        self.voz.decir("""Comandos de caecus:
                        En caecus, todos los comandos son instrucciones que se
                        escriben en una terminal de ordenes, por lo tanto es
                        necesario escribir una orden y luego presionar la tecla intro.
                        los sonidos ayudan a saber si estamos en la terminal de escritura y si
                        el sistema esta procesando la sintesis o ya termino de pronunciar.
                        Si escribes "ayuda" explica esta ayuda.
                        Si ingresas un numero hace un salto a una página especificada.
                        Si presionas la tecla más. avanza.
                        Si presionas la tecla menos. retrocede.
                        si escribes los simbolos más más, avanza una página.
                        si escribes los simbolos menos menos, retorcede una página.
                        si escribes la tecla asterisco, leera toda la página, no solo un parrafo.
                        si presionas la tecla "simbolo de pregunta" te dira información del documento.
                        si no escribes nada y presionas INTRO, vuelve a leer el parrafo.
                        Caescus puede trabajar con 3 motores de sintezis de voz.
                        Si escribes "espeak", con la letra k al final, usara el motor espeak, que viene habilitado por defecto.
                        si escribes "festival", caecus tratara de usar el motor festival.
                        si escribes "google", caecus usara el motor google, pero requiere tener internet todo el tiempo.
                        Recuerda: todos los comandos se escriben y luego debes presionar la tecla
                        INTRO para que se ejecuten, escucha los sonidos para saber si esta habilitado
                        para escribir.
                        """,self.sintesis)

class VOZ(object):

    """Docstring for VOZ. """

    def __init__(self,sintesis):
        """TODO: to be defined. """
        self.sintesis = sintesis
        self.voz = espeakng.Speaker()
        self.voz.voice="es-mx"

    def msg_promp(self):
        """TODO: Docstring for msg_ok.
        :returns: TODO

        """
        playsound.playsound("audios/promp.mp3")


    def msg_ok(self):
        """TODO: Docstring for msg_ok.
        :returns: TODO

        """
        playsound.playsound("audios/ok.mp3")

    def msg_ini(self):
        """TODO: Docstring for msg_ini.
        :returns: TODO

        """
        playsound.playsound("audios/inicio.mp3")

    def decir(self, texto,band):
        """TODO: Docstring for decir.
        :texto: TODO
        :returns: TODO
        """
        self.msg_ini()
        if band == "festival":
            os.system('echo "{0}" | iconv -f utf-8 -t iso-8859-1|festival --tts'.format(texto.rstrip()))
        elif band == "google":
            try:
                language = 'es'
                myobj = gTTS(text=texto.rstrip(), lang=language, slow=False)
                myobj.save("audios/temp.mp3")
                playsound.playsound('audios/temp.mp3')
            except:
                self.voz.say("ha ocurrido un error")
                self.voz.say("no se puede conectar al motor de voz google")
        elif band == "espeak":
            self.voz.say(texto)
            self.voz.wait()
        else:
            print("error")
        self.msg_ok()



def main(args): 
    print(len(args))
    if len(args)> 1 :
        n = NAVEGADOR(args[1],"espeak")
        print(n.promp())
    else:

        n = NAVEGADOR("","espeak")
        print(n.promp())
    return 0

if __name__ == '__main__':
    import sys
    sys.exit(main(sys.argv))
    # para deshabilitar orca usar bloq mayus + s para apagarlo



