#!/usr/bin/env python3
# -*- coding: utf-8 -*-

###############################################################################
# 
# Copyright © 2021 Valentin Basel <valentinbasel@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
###############################################################################

import os
import sys
import pathlib

class ficheros(object):

    """Docstring for ficheros. """

    def __init__(self):
        """TODO: to be defined. """
        self.__ruta = str(pathlib.Path.home()) + "/caecus/"

    def buscar(self,archivo):
        """TODO: Docstring for buscar.

        :archivo: nombre o numero de archivo para leer
        :returns: ruta del archivo pdf

        """
        pass

    def test(self):
        """ comprueba si la carpeta caecus existe en el home para crearla en 
        caso de que no este
        :returns: True
        """
        resultado = os.path.isdir(self.__ruta)
        return resultado
